using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            string connstr = string.Format("Server={0};Port={1};Database={2};User ID={3};Password={4};Pooling=true;Connection Lifetime=0;",
            Configuration.GetSection("Postgres:host").Value,
            Configuration.GetSection("Postgres:port").Value,
            Configuration.GetSection("Postgres:database").Value,
            Configuration.GetSection("Postgres:user").Value,
            Configuration.GetSection("Postgres:password").Value);


            services.AddSingleton(new DbContextOptionsBuilder().UseNpgsql(connstr).Options);

            services.AddSingleton<Socket>();
            services.AddDbContext<Context.DatabaseContext>(options =>
                options.UseNpgsql(connstr)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
            , ServiceLifetime.Scoped);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Token";
                options.DefaultChallengeScheme = "Token";
            }).AddScheme<TokenAuthOptions, TokenAuthHandler>("Token", (p) => { });

            services.AddAuthorization(configure => { });



            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Web", Version = "v1" });
            });

            services.AddCors(o => o.AddPolicy("AnyOrigin", builder =>
            {
                builder.SetIsOriginAllowed(_ => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseWebSockets();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web v1"));

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors("AnyOrigin");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
