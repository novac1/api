﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NovaChat.Context;
using NovaChat.Models.Socket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        DatabaseContext context;
        Socket socket;
        public MessageController(DatabaseContext context, Socket socket)
        {
            this.socket = socket;
            this.context = context;
        }

        [Authorize]
        [HttpGet("send")]
        public async Task<ActionResult> SendMessage(string message, string forwarded = null)
        {
            if (string.IsNullOrEmpty(message))
                return Problem("Message cant be empty");
            var userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p=>p.Messages).FirstOrDefault(p => p.Id == userid);
            long[] forw;
            if (forwarded != null)
                forw = JsonConvert.DeserializeObject<long[]>(forwarded);
            else
                forw = new long[] { };

            var addedMessage = new Models.Message
            {
                Color = user.TextColor,
                Text = message,
                Forwarded = forw.ToList()
            };
            user.Messages.Add(addedMessage);
            await context.SaveChangesAsync();
            await socket.Broadcast("chatUpdates", new NewMessage {
                color = addedMessage.Color,
                edited = addedMessage.Edited,
                forwarded = addedMessage.Forwarded.ToArray(),
                id = addedMessage.Id,
                user = addedMessage.User.Id,
                text = addedMessage.Text,
                timestamp = addedMessage.Timestamp.Ticks
            });
            return Ok();
        }
        [Authorize]
        [HttpGet("getById")]
        public async Task<ActionResult> GetMessage(long id)
        {
            var message = context.messages.FirstOrDefault(p => p.Id == id);
            return Ok(new {
                Color = message.Color,
                Edited = message.Edited,
                Forwarded = message.Forwarded,
                Id = message.Id,
                Text = message.Text,
                Timestamp = message.Timestamp.Ticks,
                User= message.User.Id
            });
        }
        [Authorize]
        [HttpGet("getByTime")]
        public async Task<ActionResult> GetMessages(DateTime fromTime)
        {
            var messages = (from b in context.messages where b.Timestamp > fromTime select b).Take(10000).Select(p => new
            {
                Color = p.Color,
                Edited = p.Edited,
                Forwarded = p.Forwarded,
                Id = p.Id,
                Text = p.Text,
                Timestamp = p.Timestamp.Ticks,
                User = p.User.Id
            });
            
            return Ok(messages.ToList());
        }
        [Authorize]
        [HttpGet("getByAmount")]
        public async Task<ActionResult> GetMessages(int amount)
        {
            var messages = context.messages.OrderBy(p=>p.Timestamp).Select(p=>
            new {
                Color = p.Color,
                Edited = p.Edited,
                Forwarded = p.Forwarded,
                Id = p.Id,
                Text = p.Text,
                Timestamp = p.Timestamp.Ticks,
                User = p.User.Id
            });

            return Ok(messages.ToList());
        }

    }
}
