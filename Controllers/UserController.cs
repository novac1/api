﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NovaChat.Context;
using NovaChat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        DatabaseContext context;
        public  UserController(DatabaseContext context)
        {
            this.context = context;
        }

        [HttpGet("register")]
        public async Task<ActionResult> Register(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return Problem("Username or password cant be null");
            var user = context.users.FirstOrDefault(p => p.Username == username);
            if (user != null)
                return Problem("User with this username already exists");
            context.users.Add(new User
            {
                Username = username,
                PasswordHash = password.getHashSha256()
            });
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpGet("getToken")]
        public async Task<ActionResult> GetToken(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return Unauthorized("Username or password cant be empty");
            var hash = password.getHashSha256();
            var user = context.users.Include(p=>p.Tokens).FirstOrDefault(p => p.Username == username && p.PasswordHash == hash);
            if (user == null)
                return Unauthorized("Invalid username or password");
            var token = user.Tokens.FirstOrDefault(p => p.isValid());
            if (token == null)
            {
                token = new Token
                {
                    ValidTo = DateTime.Now.AddDays(7),
                    Value = Extensions.RandomString(128),
                };
                user.Tokens.Add(token);
                await context.SaveChangesAsync();
            }
            return Ok(token.Value);
        }

        [Authorize]
        [HttpGet("getAvatar")]
        public FileResult GetAvatar(long? userid = null)
        {
            if (userid == null)
                userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p => p.Messages).FirstOrDefault(p => p.Id == userid);
            if (user.avatar == null)
                return File(new byte[] { }, "image/png");
            return File(user.avatar, "image/png");
        }

        [Authorize]
        [HttpPost("setAvatar")]
        public async Task<ActionResult> GetAvatar()
        {
            var userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p => p.Messages).FirstOrDefault(p => p.Id == userid);

            var stream = new StreamReader(Request.Body).BaseStream;
            using (var ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                user.avatar = ms.ToArray();
                await context.SaveChangesAsync();
            }
            return Ok();
        }
    }
}
