﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NovaChat.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NovaChat.Controllers
{
    [Route("api/socket")]
    public class WebSocketController : ControllerBase
    {

        DatabaseContext context;
        Socket socket;
        public WebSocketController(DatabaseContext context, Socket socket)
        {
            this.context = context;
            this.socket = socket;
        }

        [HttpGet("chatUpdates")]
        public async Task ChatUpdates()
        {
            if (!HttpContext.WebSockets.IsWebSocketRequest)
                HttpContext.Response.StatusCode = 401;
            WebSocket webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

            await socket.AddClient("chatUpdates", webSocket);
            return;
        }

        [HttpGet("userUpdates")]
        public async Task UserUpdates()
        {
            if (!HttpContext.WebSockets.IsWebSocketRequest)
                HttpContext.Response.StatusCode = 401;
            WebSocket webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

            await socket.AddClient("userUpdates", webSocket);
            return;
        }

    }
}
