﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models.Socket
{
    public class UpdateMessage
    {
        public string type { get; set; } = "updateMessage";

        public long id { get; set; }

        public string text { get; set; }

        public DateTime timestamp { get; set; }
    }
}
