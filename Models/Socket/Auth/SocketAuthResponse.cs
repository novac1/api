﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models.Socket.Auth
{
    public class SocketAuthResponse
    {
        public string Type { get; set; } = "AuthResponse";
        public long Code { get; set; }
        public string Text { get; set; }
    }
}
