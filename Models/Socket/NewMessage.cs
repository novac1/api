﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models.Socket
{
    public class NewMessage
    {
        public string type { get; set; } = "newMessage";

        public string color { get; set; }
        public bool edited { get; set; }
        public long[] forwarded { get; set; }

        public long id { get; set; }

        public string text { get; set; }

        public long timestamp { get; set; }

        public long user { get; set; }
    }
}
