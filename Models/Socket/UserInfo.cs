﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models.Socket
{
    public class UserInfo
    {
        public long id { get; set; }

        public string username { get; set; }

        public long lastSeen { get; set; } = DateTime.Now.Ticks;

        public string usernameColor { get; set; }
    }
}
