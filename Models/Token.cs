﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models
{
    public class Token
    {
        public long Id { get; set; }
        [MaxLength(128)]
        public string Value { get; set; }

        public DateTime ValidTo { get; set; }

        public User User { get; set; }

        public bool isValid()
        {
            return this.ValidTo > DateTime.Now;
        }
    }
}
