﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NovaChat.Models
{
    public class Message
    {
        [Key]
        public long Id { get; set; }
        public User User { get; set; }

        public DateTime Timestamp { get; set; } = DateTime.Now;

        public string Color { get; set; }

        public string Text { get; set; }

        public bool Edited { get; set; } = false;

        public List<long> Forwarded
        {
            get => JsonConvert.DeserializeObject<List<long>>(_Forwarded);
            set => _Forwarded = JsonConvert.SerializeObject(value);
        }

        public string _Forwarded { get; set; }

    }
}
