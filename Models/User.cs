﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NovaChat.Models
{
    public class User
    {
        [Key]
        public long Id { get; set; }

        public string Username { get; set; }
        public string PasswordHash { get; set; }

        public byte[] avatar { get; set; }

        public string UsernameColor { get; set; } = "#ffffff";
        public string TextColor { get; set; } = "#ffffff";

        public List<Message> Messages { get; set; }
        public List<Token> Tokens { get; set; }

        public bool Validate(string Username, string Password) => this.Username == Username && this.PasswordHash == Password.getHashSha256();
    }
}
