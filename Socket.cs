﻿using Microsoft.AspNetCore.Connections.Features;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NovaChat.Context;
using NovaChat.Models.Socket;
using NovaChat.Models.Socket.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NovaChat
{
    public class WebSocketUser
    {
        public WebSocket socket;
        public long? userid;
    }
    public class Socket
    {
        DbContextOptions<DatabaseContext> contextoptions;

        public Socket(DbContextOptions<DatabaseContext> contextoptions)
        {
            this.contextoptions = contextoptions;
        }

        public Dictionary<string, Dictionary<long?, List<WebSocketUser>>> loggedClients = new Dictionary<string, Dictionary<long?, List<WebSocketUser>>>();

        public Dictionary<string, List<WebSocket>> unloggedClients = new Dictionary<string, List<WebSocket>>();

        public async Task AddClient(string name, WebSocket webSocket)
        {
            if (unloggedClients.ContainsKey(name))
            {
                unloggedClients[name].Add(webSocket);
            }
            else
            {
                unloggedClients.Add(name, new List<WebSocket> { webSocket });
                loggedClients.Add(name, new Dictionary<long?, List<WebSocketUser>>());
            }

            while (webSocket.State == WebSocketState.Open)
            {
                var buffer = new byte[1024 * 4];
                WebSocketReceiveResult message = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (message.Count == 0)
                    continue;
                var data = new byte[message.Count];
                Array.Copy(buffer, 0, data, 0, message.Count);
                await HandleMessage(name, webSocket, data);
            }
            unloggedClients[name].RemoveAll(p => p.State != WebSocketState.Open);
        }
        private async Task HandleMessage(string socketType, WebSocket wsinfo, byte[] buffer)
        {
            SocketAuthResponse response = new();
            try
            {
                string data = Encoding.UTF8.GetString(buffer);
                if (string.IsNullOrEmpty(data))
                    return;
                JObject json = JObject.Parse(data);
                if (json["Type"].Value<string>() == "AuthRequest")
                {
                    string token = json["Token"].Value<string>();
                    using (var context = new DatabaseContext(contextoptions))
                    {
                        var tokenObject = context.tokens.Include(p => p.User).FirstOrDefault(p => p.Value == token);
                        if (tokenObject == null)
                        {
                            response.Code = 401;
                            response.Text = "Invalid token";
                        }
                        else
                        {
                            response.Code = 200;
                            response.Text = "Successfully authtorized";
                            
                            var wsuser = new WebSocketUser
                            {
                                userid = tokenObject.User.Id,
                                socket = wsinfo
                            };

                            unloggedClients[socketType].Remove(wsinfo);

                            if (!loggedClients[socketType].ContainsKey(wsuser.userid))
                                loggedClients[socketType][wsuser.userid] = new List<WebSocketUser>();
                            loggedClients[socketType][wsuser.userid].Add(wsuser);

                            var usersinfo = this.loggedClients["chatUpdates"].Keys.ToList().Select(p => context.users.FirstOrDefault(i => i.Id == p)).Select(p => new UserInfo
                            {
                                id = p.Id,
                                lastSeen = DateTime.Now.Ticks,
                                username = p.Username,
                                usernameColor = p.UsernameColor
                            }).ToList();
                            await Broadcast("userUpdates", usersinfo);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                response.Code = 500;
                response.Text = e.Message;
            }
            return;
        }

        public async Task Broadcast(string name, object message)
        {
            if (!loggedClients.ContainsKey(name))
                return;
            var data = new ArraySegment<byte>(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));

            foreach (var p in loggedClients[name]) {
                foreach (var k in p.Value) {
                    if (k.socket.State == WebSocketState.Open)
                        await k.socket.SendAsync(data, WebSocketMessageType.Text, true, CancellationToken.None);
                }
            }
        }

    }
}
